#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on 2021/10/27
This code defines the erddap generator. Some basic functions for the generator include:

@author: rory
"""
#Standard Libs
from typing import OrderedDict
import os
import logging 
import jinja2
import pandas as pd
import requests
import re
import datetime
#Non-Standard Libs
import pymssql 

from  erddap_gen import eurobis_template

log = logging.getLogger('erddap_gen') 

def eurobis_query(sql):
    log.debug(' -Performing SQL query...')
    server = os.getenv('DB_SERVER','sql17stage.vliz.be')
    user = os.getenv('DB_USER','anyone')
    password = os.getenv('DB_PASSWORD','')
    db = os.getenv('DB_DB','eurobis')  

    conn = pymssql.connect(server, user, password, db)
    cursor = conn.cursor(as_dict=True)
    cursor.execute(sql)
    dataset_ids=cursor.fetchall()
    log.debug('      - Fetched data...')
    column_names= [item[0] for item in cursor.description]
    data_df=pd.DataFrame(dataset_ids, columns=column_names) #.dropna(axis=1, how='all')
    log.debug('      - Converted to pandas df...')

    log.debug(' -Returning {0} rows from query'.format(len(data_df)))

    return data_df

def get_dataset_sql():
    sql = '''SELECT  
                id as dataprovider_id,
                name  
            FROM dataproviders as aa
            WHERE aa.core = 1
            AND aa.active = 1  
            and id = 534 '''
    return sql


def get_datasets():
    dataset_df = eurobis_query(get_dataset_sql())
    return dataset_df


class ErddapDataSet:
    '''
    Takes a eurobis ID and generates all the info that you'd need to create
    a erddap datasets.xml snippet and the corrosponding csv file. 
    '''
    def __init__(self,eurobis_id):
        log.debug('----INIT----')
        self.good_dataset = True
        self.eurobis_id = eurobis_id
        
        # =======
        # Get Dataset Metadata
        # ======= 
        if self.good_dataset:
            try:
                self.get_dataset_metadata()
            except Exception as err:
                log.warning(err)


        # =======
        # Get File/Path/Timestamp/Template data
        # =======
        
        # self.generate_id()  
        self.check_paths()   

        # =======
        # Get Dataset data
        # =======
        if self.good_dataset:
            self.get_dataset_data()

        # =======
        # Convert Data to output format
        # =======
        if self.good_dataset:
            self.convert_data()

        # =======
        # Write Metadata and Data to files
        # =======
        if self.good_dataset:
            self.to_xml()       
            self.data_to_csv()

        for root, dirs, files in os.walk(os.getenv('SNIPPET_DIR','/erddapData/')):  
            for momo in dirs:  
                os.chmod(os.path.join(root, momo), 0o777)
            for momo in files:
                os.chmod(os.path.join(root, momo), 0o777)

    def generate_id(self):
        '''
        Create an erddap ID from the eurobis_ID
        '''
        dataset_prefix = os.getenv('DATASET_PREFIX','eurobis_dataprovider_')
        erddap_id = dataset_prefix + str(self.eurobis_id)
        log.debug(' -generate dataset ID: {0}'.format(erddap_id))

        self.id = erddap_id
        return 
 
    def check_paths(self):
        '''
        Create the path by checking the .env file specified in the docker-compose project
        Also check if the files already exists and when last it was created. 
        ''' 
        local_path = os.path.join(os.getenv('SNIPPET_DIR','/erddapData/producer_datasets'), os.getenv('EUROBIS_SUBDIR','eurobis_producer/'), self.id)
        if os.path.exists(local_path):
            last_mod_epoch = os.path.getmtime(local_path)
            self.file_last_mod = datetime.datetime.fromtimestamp(last_mod_epoch)
            log.debug('Erddap path exists: {0}'.format(local_path))
            log.debug('Last Modified at: {0}'.format(self.file_last_mod))
            if self.file_last_mod < datetime.datetime.now(): #What is the point of this line?
                self.create_erddap_flag = True
                log.debug('New erddap object required.')
            else:
                self.create_erddap_flag = False
                self.good_dataset = False
                log.debug('New erddap object NOT required.')
        else:
            log.debug('Erddap path does not exist: {0}'.format(local_path))
            log.debug('New erddap object required.')
            self.create_erddap_flag = True
            os.makedirs(local_path) 
            os.chmod(local_path, 0o777)
 
        log.debug(' -container file path: {0}'.format(local_path))
        self.file_path = local_path
        return  
    
    def remove_html_tags(self, in_str):
        '''
        There are some items, pulled from IMIS, that have lots of HTML tags in them. This breaks the 
        erddap xml. Not great. 
        '''
        CLEANR = re.compile('<.*?>')
        out_str = re.sub(CLEANR, '', str(in_str))
        return out_str

    def get_dataset_metadata(self):
        '''
        Fetch data from eurobis for the specified eurobis_id
        TODO: figure out a better way to handle the JSON schema. There are too many none's and if/then's
        '''
        dataprovider_df = eurobis_query(self.sql_dataprovider())
        self.imis_url = os.getenv('IMIS_URL', 'https://www.vliz.be/en/imis')
        self.imis_params = dict(module='dataset',dasid='',show='json')
        self.imis_params['dasid'] = dataprovider_df['IMIS_DasID']
        imis_req = requests.get(url=self.imis_url, params=self.imis_params, timeout=60) 
        log.debug(' -getting data from {0}'.format(imis_req.url))
        imis_data = imis_req.json() 
 
        # self.author = imis_data.get('refs',{}).get('RSA')
        # self.infoUrl = imis_data.get('dois')[0].get('URL',imis_req.url)
        # self.institution = imis_data.get('ownerships',[{}])[0].get('FullAcronym')
        # self.summary = self.remove_html_tags(imis_data.get('datasetrec',{}).get('EngAbstract'))
        # self.title = self.remove_html_tags(imis_data.get('datasetrec',{}).get('StandardTitle'))
        # self.id = dataprovider_df.name
        # self.keywords = self.remove_html_tags(imis_data.get('keywords'))
        self.infoUrl = imis_data.get('dois')
        try:
            acr_set = list(set([x.get('Surname') for x in imis_data.get('ownerships') if x.get('Surname') is not None] ))
            self.author = ', '.join(acr_set) 
        except:
            self.author = 'None'

        try:
            acr_set = list(set([x.get('FullAcronym') for x in imis_data.get('ownerships') if x.get('FullAcronym') is not None] ))
            self.institution = ', '.join(acr_set) 
        except:
            self.institution = 'None'
        if self.institution is None or self.institution == '':
            self.institution = 'None'

        dataset_rec = imis_data.get('datasetrec')
        if dataset_rec is not None:
            self.summary = self.remove_html_tags(imis_data.get('datasetrec',{}).get('EngAbstract'))
            self.title = self.remove_html_tags(imis_data.get('datasetrec',{}).get('StandardTitle'))
        else:
            self.summary = ''
            self.title = ''
        
        self.id = str(dataprovider_df.name.values[0]).strip()
        self.keywords = self.remove_html_tags(imis_data.get('keywords'))
        self.attributes = {key:self.remove_html_tags(value) for key, value in imis_data['datasetrec'].items()}
        log.debug('Fetched data for {0}'.format(self.id))

        return

    def pandas_to_ed_datatypes(self, pdType):
        '''
        Convert the pandas column datatypes to erddap datatypes
        '''
        pd_to_ed_dict = {'string':'String',
                        'object':'String',
                        'float64':'float',
                        'float32':'float',
                        'float16':'float',
                        'int':'long',
                        'category':'String'}
        erd_type = pd_to_ed_dict.get(str(pdType), 'String')
        log.debug(' -converting {0} to erddap data type: {1}'.format(pdType,erd_type))
        return erd_type

    def get_dataset_data(self):
        '''
        Fetch the actual data from the DB. Return a list of dicts that contain the column metadata + data
        TODO: get keywords in a better way. IMIS? 
        TODO: remove the defaults from the secrets
        '''
        log.debug('Getting occurance data...')
        # self.data_template = eurobis_template.Eurobis_Simple_Data(self.eurobis_id)
        # self.data_template = eurobis_template.Eurobis_MOF_Data(self.eurobis_id)
        self.data_template = eurobis_template.Eurobis_MOF_Pivot_Data(self.eurobis_id)
        
        self.data_df = self.data_template.data_df
        if self.data_df is None:
            log.warning('Skipping this data set')
            self.good_dataset = False
            return

    def convert_data(self):
        '''
        Here is where the SQL return gets changed to be the ERDDAP tabular data. Attributes and datatypes 
        are stored in the variable dict, which gets turned into the xml snippet. 

        TODO: THis should probably get broken up into the "turn into table frame" and "get xml parameters out" 
        '''

        self.data_df = self.data_df.dropna(how='all', axis=1)  

        data_uri = self.data_template.uri_columns
        data_subsets = self.data_template.column_subsets
        subsets = []
        for column_name in self.data_df: 
            if data_subsets.get(column_name) is True:
                subsets.append(column_name)
        self.subsets = ','.join(str(x) for x in subsets)
        self.variables = []
        for column in self.data_df: 
            variable_dict = {}
            variable_dict['attributes'] = {'units_uri':data_uri.get(column)}
            variable_dict['units'] = data_uri.get(column)

            if 'vocab.nerc.ac.uk' in column:
                log.debug('Possible nerc vocab term: {0}'.format(column))
                nerc_json = requests.get(url=column, headers={"Accept":"application/ld+json"})  
                try:
                    json_dict = nerc_json.json()
                    variable_dict['attributes']['altLabel'] = json_dict['altLabel']
                    variable_dict['attributes']['sdn_parameter_urn'] = json_dict['identifier'] 
                    variable_dict['attributes']['uri'] = column
                    # self.data_df = self.data_df.rename(columns={column:json_dict['identifier']})
                    variable_dict['sourceName'] = self.data_df[column].name
                    variable_dict['destinationName'] = json_dict['identifier']
                    if isinstance(json_dict['altLabel'],list):
                        variable_dict['destinationName'] = json_dict.get('altLabel')[0]
                    else:
                        variable_dict['destinationName'] = json_dict.get('altLabel')
                    if variable_dict['destinationName'] == '':
                        log.debug('NERC Vocab missing altLabel...')
                        variable_dict['destinationName'] = json_dict.get('prefLabel').get('@value').replace(' ','_')
                    variable_dict['prettyName'] = json_dict['identifier']
                except Exception as err:
                    log.warning('Problem with NERC vocab translation: ')
                    log.warning(err) 
            else:
                variable_dict['sourceName'] = self.data_df[column].name
                variable_dict['destinationName'] = self.data_df[column].name
                variable_dict['prettyName'] = variable_dict['sourceName']

            if column == 'time':
                variable_dict['attributes'] = {'units_uri':data_uri.get(column),
                                               'time_precision':'1970-01-01T00:00:00Z'}
                variable_dict['units'] = "yyyy-MM-dd'T'HH:mm:ss"
 
                # <dataVariable>
                #         <sourceName>time</sourceName>
                #         <destinationName>time</destinationName>
                #         <dataType>String</dataType>
                #         <!-- sourceAttributes>
                #         </sourceAttributes -->
                #         <addAttributes>
                #             <att name="long_name">Time</att>
                #             <att name="standard_name">time</att>
                #             <att name="time_precision">1970-01-01T00:00:00Z</att>
                #             <att name="units">yyyy-MM-dd&#39;T&#39;HH:mm:ss</att>
                #         </addAttributes>
                #     </dataVariable> -->

                # <dataVariable>
                # <sourceName>time</sourceName>
                # <destinationName>time</destinationName>
                # <dataType>String</dataType>
                # <addAttributes>
                # <att name="long_name">time</att>
                # <att name="units">yyyy-MM-dd&#39;T&#39;HH:mm:ss</att>
                # <att name="units_uri">None </att>
                # <att name="time_precision">1970-01-01T00:00:00Z </att>
                # </addAttributes>

            variable_dict['pdType'] = self.data_df[column].dtype
            variable_dict['erddapType'] = self.pandas_to_ed_datatypes(variable_dict['pdType'])
            
            self.variables.append(variable_dict) 
            
        
        
        self.columnNames = ','.join(str(x) for x in self.data_df.columns)
        self.keywords = ','.join(str(x) for x in self.data_df.columns)
        
        return 

    def to_xml(self):
        '''
        Writes the dataset to the XML file.
        ''' 
        templateLoader = jinja2.FileSystemLoader(searchpath="/code/templates/")
        templateEnv = jinja2.Environment(loader=templateLoader)
        template = templateEnv.get_template('dataset_body.xml')
        snippet_xml = template.render(dataset=self)
        log.debug(' -generating snippet XML...')
        # log.debug(self.variables)
        if self.create_erddap_flag:
            log.debug(' -saving xml file to : {0}'.format(self.file_path))
            with open(os.path.join(self.file_path,'snippet.xml'), "w+") as fh:
                fh.write(snippet_xml)
            os.chmod(os.path.join(self.file_path,'snippet.xml'), 0o777)
        return 

    def data_to_csv(self):
        self.data_df.to_csv(os.path.join(self.file_path, self.id + '.csv'))
        log.debug(' -saving data file to : {0}'.format(os.path.join(self.file_path, self.id + '.csv')))

    def sql_dataprovider(self):
        '''
        The SQL to use to get the dataset information
        '''
        sql = 'SELECT * FROM dataproviders WHERE id = {0};'.format(self.eurobis_id)
        log.debug(' -get dataprovider with SQL: {0}'.format(sql))
        return sql
 