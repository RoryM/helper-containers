# Helper Tools
Here are some helper tools that I like to use on machines as I go about. 
Currently there are:
* PGAdmin: for talking to postgresql databases
* CloudBeaver: for talking to non postgresql databases
* Jupyter Notebooks: the datascience flavoured one, for doing some quick python'ing

## Usage
```
git clone git@gitlab.com:RoryM/helper-containers.git
cd ./helper-containers
cp ./config/sample.env .env
docker-compose up -d
```

## Environment
To setup your environment copy ./config/sample.env to .env.  and set the ports, tags etc to reflect your system and desires

## Persistance
There are several docker volumes specced to save things like PG server details. There is also one mounted volume in order to save/import/export data to the notebook container. 

## Security
I run this in a secure environment so my own comfort I've switched off most user/password on the containers. I wouldn't normally recommend doing that... 
